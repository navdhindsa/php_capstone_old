<?php
  /**
  * Capstone
  * @file footer.inc.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
      <div id="top">
        <a href="#" title="Go to Top"><img src="images/top.png" alt="Top"></a>
      </div>
      <footer>
      
        <div id="left_footer">
          <p>SITEMAP</p>
          <ul>
          <li><a href="index.html" title="Home" >HOME</a></li>
              <li><a href="about.html" title="About" >ABOUT</a></li><!--Services-->
              <li><a href="skills.html" title="Skills" >SKILLS</a></li><!--about us-->
              <li><a href="faqs.html" title="FAQs">FAQs</a></li><!--contact us-->
              <li><a href="connect.html" title="Connect">CONNECT</a></li><!--connect-->  
          </ul>
        </div>  <!-- left-footer ends  -->  
        
        <div id="center_footer"> 
          <p>Copyright &copy; 2018 Navdeep. All rights reserved.</p>
        </div>  <!--  center_footer ends -->  
        
        <div id="right_footer">
          <p>Find us on Social Media:</p>
          <span class="social">
            <a href="https://www.facebook.com/" title="Facebook"><img src="images/facebook.png" alt="Facebook" style="width: 40px; height: 40px" /></a>
          </span>
          <span class="social">
            <a href="https://www.twitter.com/" title="twitter"><img src="images/twitter.png" alt="Twitter" style="width: 40px; height: 40px" /></a>
          </span>
          <span class="social">
            <a href="https://www.instagram.com/" title="Instagram"><img src="images/instagram.png" alt="Instagram+" style="width: 40px; height: 40px" /></a>
          </span>
            
        </div>    <!-- right_footer ends  -->  
      
      
      </footer>
    </div><!-- wrapper ends-->
  </body>
</html>
