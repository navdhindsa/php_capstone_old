
<?php
  /**
  * Capstone
  * @file nav.inc.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
  <div id="wrapper">
      
      <header>
        <div id="header_left">
        <span id="logo">
          <img src="images/logonew.JPG" width="175" height="140" alt="Logo" />
        </span>
        
        <div id="tagline">
          <h1>
          <?=$title?>
          </h1>
        </div>
      </div> <!-- left header ends-->
        
        <nav>
          <div id="menu">
            <a href="#" id="menulink" title="Menu">
              <div class="bar1"></div>
              <div class="bar2"></div>
              <div class="bar3"></div>
            </a>
             
            <ul id="navlist">
              <li><a href="index.php"  <?php if($slug == 'index') {echo 'class="current"';}?> title="Home">HOME</a></li><!--Home-->
              <li><a href="about.php"   <?php if($slug == 'about') {echo 'class="current"';}?>  title="About">ABOUT</a></li><!--Services-->
              <li><a href="skills.php"   <?php if($slug == 'skills') {echo 'class="current"';}?>  title="Skills">SKILLS</a></li><!--about us-->
              <li><a href="faqs.php"   <?php if($slug == 'faqs') {echo 'class="current"';}?>  title="FAQs">FAQs</a></li><!--contact us-->
              <li><a href="connect.php"   <?php if($slug == 'connect') {echo 'class="current"';}?>  title="Connect">CONNECT</a></li><!--reviews-->
            </ul>
          </div>
        </nav>
      </header>