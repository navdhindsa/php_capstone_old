<?php 

$title = 'Navdeep - About';
$slug = 'about';
require '../config.php';
include '../includes/header.inc.php'; 

?>
<?php
  /**
  * Capstone
  * @file about.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
  <body id="about">
    <?php include '../includes/nav.inc.php' ?>  
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
        <h2>My passions and personality</h2>
        <p id="first">Discover who I am as a person aside from being a developer. </p>
      </div><!-- Content ends -->
      <div id="aboutme">
        
        <div id="aleft"><br>
          <a href="#love" title="Things I love"><img src="images/heart.png" alt="heart" width="250" height="250"/></a>
          <span class="text" style=" color: #35afbe; font-weight: 700;">LOVE</span>
        </div>
        
        <div id="acenter">
          <a href="#core" title="Core Info About me"><img src="images/brain.png" alt="brain" width="250" height="250"/></a>
          <span class="text" style=" color: #f7941e; font-weight: 700;">CORE</span>
        </div>
        
        <div id="aright">
          <a href="#work" title="My Professional Info"><img src="images/work.png" alt="computer" width="250" height="250"/></a>
          <span class="text " style=" color: #35afbe; font-weight: 700;">WORK</span>
        </div>
     
      </div><!-- aboutme ends-->
      
      <div id="love">
        <h3>Things I love</h3>
        
        <div class="slide">
          <img src="images/books.png" alt="Slide 4" />
          <img src="images/family.jpg" alt="Slide 3" />
          <img src="images/dogs.jpg" alt="Slide 2" />
          <img src="images/harrypotter.png" alt="Slide 1" />
        </div>
      </div><!-- love ends-->
      
      <div id="core">
        <h3>Core Info About Me</h3>
        <p>Design and development are both significant areas and in digital era, one can not exist without the other</p>
        <p>I am passionate about creating new things logically, and in age of digitalization I believe bringing design to life with code is essential.</p>
        <p>A full-time student, I'm 23 and currently based in Winnipeg, Manitoba. </p>
        <p>I have completed my Bachelor degree in Computer Science Engineering and currently study Web Development at the University of Winnipeg, Manitoba.</p>
      
      
      
      </div><!-- core ends-->
      
      <div id="work">
        <h3>My Work Ethics</h3>
        <div id="slide2">
          <img src="images/html.PNG" alt="Slide 4" />
          <img src="images/java.PNG" alt="Slide 3" />
          <img src="images/focus.PNG" alt="Slide 2" />
          <img src="images/side.PNG" alt="Slide 1" />
        </div>
      </div><!-- work ends-->
      
      <?php include '../includes/footer.inc.php' ?>
