<?php 
$title = 'Navdeep Dhindsa - Official Website';
$slug = 'index';
require '../config.php';
include '../includes/header.inc.php'; 

?>
<?php
  /**
  * Capstone
  * @file index.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
  <body id="home">
    
   <?php include '../includes/nav.inc.php' ?>
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <div id="hero">
          <h2> Welcome to the official website of Navdeep Dhindsa!!</h2>
        </div> 
        <div>
          <div id="myself">
            <h3>Hi, I’m Navdeep! </h3>
            <p>I like coding and meeting new people.</p>
            <p>Intrigued by code, traveling, photography, music, reading, writing, art, fabulous food, and even better conversations.</p>
            <p>Seeking to be inspired, to envision the unlikely, to work hard for things that are worth it, and to be surrounded by those who bring out the best in me.</p>
            <p>Graduate in Computer in science and currently studying Web Development at University of Winnipeg, Canada.</p>
            <p>Browse this website to know more about me or to get in touch.</p>
          </div>
        </div>
      </div>
      
         <?php include '../includes/footer.inc.php' ?>
      

