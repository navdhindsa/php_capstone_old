<?php 

$title = 'Navdeep - Connect with Us';
$slug = 'connect';
require '../config.php';
include '../includes/header.inc.php'; 

?>
<?php
  /**
  * Capstone
  * @file connect.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
  <body id="connect">
   <?php include '../includes/nav.inc.php' ?>    
          <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <h2>Have an exciting project you are interested in working together on? Just want to say hi? Get in touch. </h2>
        <form id="email"
              method="post"
              action="http://www.scott-media.com/test/form_display.php"
              name="email"
              autocomplete="on" >
          <fieldset>
            <legend>Send an Email</legend>
            <p>
              <label for="first_name">First Name <span class="required">*</span></label>
              <input type="text"
                id="first_name" 
                name="first_name" 
                maxlength="25"
                size="30"
                placeholder="First Name"
                required/>
            </p>
            <p>
              <label for="last_name">Last Name <span class="required">*</span></label>
              <input type="text"
                id="last_name" 
                name="last_name" 
                maxlength="25"
                size="30"
                placeholder="Last Name"
                required/>
            </p>
            <p>
              <label for="email_address">Email Address <span class="required">*</span></label> 
              <input type="email" 
                     name="email_address" 
                     id="email_address"
                     required/>
            </p>
            <p>
              Please choose your age range.<br />

              <input type="radio"
                     name="age"
                     id="age0-15"
                     value="0-15" />
              <label for="age0-15">0 to 15 years</label><br />

              <input type="radio"
                     name="age"
                     id="age16-30"
                     value="16-30" />
              <label for="age16-30">16 to 30 years</label><br />

              <input type="radio"
                     name="age"
                     id="age31-55"
                     value="31-55" />
              <label for="age31-55">31 to 55 years</label><br />

              <input type="radio"
                     name="age"
                     id="age56-70"
                     value="56-70" />
              <label for="age56-70">56 - 70 years</label><br />

              <input type="radio"
                     name="age"
                     id="age71_plus"
                     value="71_plus" />
              <label for="age71_plus">71 and up years</label><br />          
            </p>
            <p>
              <label for="comments">Please leave your comments</label>  <br />
              <textarea cols="30" 
                        rows="6" 
                        id="comments" 
                        name="comments">
              </textarea>
            </p>
            <p>
              <input type="submit" 
                     name="submit" 
                     id="submit" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset" 
                     id="reset" 
                     value="Reset" 
              />
              </p>

          </fieldset>
        </form>

        <h2>Prefer a more personal chat? Book an appointment for a call.</h2>
        <form id="call"
              method="post"
              action="http://www.scott-media.com/test/form_display.php"
              name="call"
              autocomplete="on"
              >
          <fieldset>
            <legend>Book an appointment</legend>
            <p>
              <label for="first_name1">First Name <span class="required">*</span></label>
              <input type="text"
                id="first_name1" 
                name="first_name1" 
                maxlength="25"
                size="30"
                placeholder="First Name"
                Required/>
            </p>
            <p>
              <label for="last_name1">Last Name <span class="required">*</span></label>
              <input type="text"
                id="last_name1" 
                name="last_name1" 
                maxlength="25"
                size="30"
                placeholder="Last Name"
                Required/>
            </p>
            <p>
              <label for="phone">Telephone <span class="required">*</span></label>
              <input type="tel"
                     id="phone"
                     name="phone"
                     maxlength="18"
                     Required />
            </p>
            <p>
              <label for="pdate">Choose your preferred date</label>
              <input type="date"
                   id="pdate"
                   name="pdate" />
            </p>
            <p>
              <input type="submit" 
                     name="submit1" 
                     id="submit1" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset1" 
                     id="reset1" 
                     value="Reset" 
              />
              </p>

          </fieldset>

        </form>

      </div>
      <?php include '../includes/footer.inc.php' ?>

