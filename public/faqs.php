<?php 

$title = 'Navdeep -FAQs';
$slug = 'faqs';
require '../config.php';
include '../includes/header.inc.php'; 

?> 
<?php
  /**
  * Capstone
  * @file faqs.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
  <body id="faqs">
   <?php include '../includes/nav.inc.php' ?>    
    
      <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->

        <!-- Main content starts-->  
        <section>
          <h2>Frequently Asked Questions..</h2>
          <p>Please check these out before sending me a note. I may have covered your question already. Thanks!</p>
        </section>

        <div class="question">
          <h3>What are the languages I know?</h3>
          <p>I know English, Hindi, Punjabi.</p>
        </div>
        <div class="question">
          <h3>What are the technical skills I have?</h3>
          <p>I know HTML, JAVA, JavaScript, PHP, little bit of photoshop.</p>
        </div>
        <div class="question">
          <h3>Am I available for personal meetings?</h3>
          <p>Sorry, I have a busy schedule right now, but maybe in future...</p>
        </div>
        <div class="question">
          <h3>Can I use your illustrations/writing on my site?</h3>
          <p>Not without my permission. To license images or syndicate my writing, please get in touch with what you are interested in using, where, and to what audience. If I do not respond, please assume the image or article is not available for you to reproduce.</p>
        </div>
        <div class="question">
          <h3>Can you critique my product / website / code / something I'm working on?</h3>
          <p>Please know that I have a busy schedule, so it depends on my availability. Do send me an email if you want to discuss further.</p>
        </div>
      </div>
       <?php include '../includes/footer.inc.php' ?>

