<?php 

$title = 'Navdeep - Skills';
$slug = 'skills';
require '../config.php';
include '../includes/header.inc.php'; 

?>
<?php
  /**
  * Capstone
  * @file skills.php
  * @course Intro PHP, WDD 2018 Jan
  * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
  * @created_at 2018-08-02
  */
?>
  <body id="skills">
   <?php include '../includes/nav.inc.php' ?>    
         <div id="content">

      <!-- warning message for IE9 and earlier --> 
      <!--[if LT IE 9]>
        <div class="row">
          <div id="old_ie_warning"style="position:absolute; top:0px; ">
            <p>Some of the new features are not available for your broswer. Please upgrade it!</p>
          </div>
        </div>
      <![endif]-->
        <h2>Following are the skills I have:</h2>
        <div id="obox">
          <div class="box">
            <h3>?</h3>  
            <p>HTML/CSS</p>
          </div>

          <div class="box">
            <h3>?</h3>  
            <p>JAVA</p>
          </div>

          <div class="box">
            <h3>?</h3>  
            <p>PHP</p>
          </div>

          <div class="box">
            <h3>?</h3>  
            <p>Photoshop</p>
          </div>
        </div><!-- outer box ends-->
      
      <div id="tab">
        <h2>Check if you have the required skills for Website Development??</h2>
        <table >
          <caption>
            Skills required for Website Development
          </caption>
          <tbody>
            <tr>
            <th>Skills</th>
            <th>Required?</th>
          </tr>
          <tr>
            <th>HTML</th>
            <td>Yes</td>
            
          </tr>
          <tr>
            <th>CSS</th>
            <td>Yes</td>
            
          </tr>
          <tr>
            <th>JavaScript</th>
            <td>Yes</td>
            
          </tr>
          <tr>
            <th>JAVA</th>
            <td>Yes</td>
            
          </tr>
            <tr>
            <th>PHP</th>
            <td>Yes</td>
            
          </tr>
            <tr>
            <th>Dancing</th>
            <td>Not Really</td>
            
          </tr>
        </tbody>
        </table>

      </div><!--Table ends-->
      </div><!-- Content Ends-->
    
  

      
      <div id="top">
        <a href="#" title="Go to Top"><img src="images/top.png" alt="Top"></a>
      </div>
      <footer>
      
        <div id="left_footer">
          <p>SITEMAP</p>
          <ul>
          <li><a href="index.html" title="Home" >HOME</a></li>
              <li><a href="about.html" title="About" >ABOUT</a></li><!--Services-->
              <li><a href="skills.html" title="Skills" >SKILLS</a></li><!--about us-->
              <li><a href="faqs.html" title="FAQs">FAQs</a></li><!--contact us-->
              <li><a href="connect.html" title="Connect">CONNECT</a></li><!--connect-->  
          </ul>
        </div>  <!-- left-footer ends  -->  
        
        <div id="center_footer"> 
          <p>Copyright &copy; 2018 Navdeep. All rights reserved.</p>
        </div>  <!--  center_footer ends -->  
        
        <div id="right_footer">
          <p>Find us on Social Media:</p>
          <span class="social">
            <a href="https://www.facebook.com/" title="Facebook"><img src="images/facebook.png" alt="Facebook" style="width: 40px; height: 40px" /></a>
          </span>
          <span class="social">
            <a href="https://www.twitter.com/" title="twitter"><img src="images/twitter.png" alt="Twitter" style="width: 40px; height: 40px" /></a>
          </span>
          <span class="social">
            <a href="https://www.instagram.com/" title="Instagram"><img src="images/instagram.png" alt="Instagram+" style="width: 40px; height: 40px" /></a>
          </span>
            
        </div>    <!-- right_footer ends  -->  
      
      
      </footer>
    </div>
  </body>
</html>

